package io.kontak.apps.temperature.generator.config;

import io.kontak.apps.event.TemperatureReading;
import io.kontak.apps.temperature.generator.ParameterizedTemperatureGenerator;
import io.kontak.apps.temperature.generator.TemperatureStreamPublisher;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorMode;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorParameters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;

import java.util.function.Supplier;

@Configuration
public class TemperatureGeneratorConfig {
    @Bean
    public ParameterizedTemperatureGenerator parameterizedTemperatureGenerator() {
        return new ParameterizedTemperatureGenerator(
                new TemperatureGeneratorParameters(20, 3, 14, 4,
                        2, 5, 2, TemperatureGeneratorMode.FRAME_SERIES));
    }
}
