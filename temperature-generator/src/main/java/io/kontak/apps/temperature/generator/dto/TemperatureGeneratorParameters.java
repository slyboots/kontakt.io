package io.kontak.apps.temperature.generator.dto;

public record TemperatureGeneratorParameters(double meanValue, int roomNumbers, int thermometerNumbers, int dataSeries,
                                             int anomalyOccurrences,double maxAllowDiff,int readingTimeFrequency, TemperatureGeneratorMode mode) {
}
