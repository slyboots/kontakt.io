package io.kontak.apps.temperature.generator;

import io.kontak.apps.event.TemperatureReading;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class TemperatureGeneratorRoom {
    private final int thermometerNumbers;
    private final int readingTimeFrequency;
    private final double meanValue;
    private final double maxAllowDiff;
    private final String roomId;
    private final LocalDateTime start;
    private final List<TemperatureGeneratorThermometer> thermometerList = new ArrayList<>();

    private final Random random = new Random();

    public TemperatureGeneratorRoom(String roomId , int thermometerNumbers, double meanValue, double maxAllowDiff, int readingTimeFrequency, LocalDateTime start) {
        this.thermometerNumbers = thermometerNumbers;
        this.meanValue = meanValue;
        this.maxAllowDiff = maxAllowDiff;
        this.readingTimeFrequency=readingTimeFrequency;
        this.roomId= roomId;
        this.start=start;
        initThermometers();
    }
    private void initThermometers() {
        this.thermometerList.addAll(IntStream.range(0, thermometerNumbers)
                .mapToObj(id -> new TemperatureGeneratorThermometer(UUID.randomUUID().toString(), roomId, start,
                        meanValue + (maxAllowDiff * (random.nextDouble() - 0.5)), this.maxAllowDiff / 2, readingTimeFrequency)).toList());


    }
    public List<TemperatureReading> generate(){
        return thermometerList.stream().map(TemperatureGeneratorThermometer::generateNext).toList();
    }
}
