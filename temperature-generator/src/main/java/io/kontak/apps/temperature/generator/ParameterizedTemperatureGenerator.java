package io.kontak.apps.temperature.generator;

import io.kontak.apps.event.TemperatureReading;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorMode;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorParameters;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class ParameterizedTemperatureGenerator implements  TemperatureGenerator {
    private List<TemperatureGeneratorRoom> roomList = new ArrayList<>();
    private int roomNumbers =1 ;
    private int dataSeries = 1 ;
    private int thermometerNumbers = 1;
    private double meanValue = 20 ;
    private TemperatureGeneratorMode mode=TemperatureGeneratorMode.FRAME_SERIES;
    private int anomalyOccurrences=0;
    private int readingTimeFrequency =2;
    private double maxAllowDiff = 5;
    private final Random random = new Random();
    private LocalDateTime start;

    public ParameterizedTemperatureGenerator (TemperatureGeneratorParameters temperatureGeneratorParameters) {
        if(temperatureGeneratorParameters.roomNumbers()<1) throw new BadTemperatureGeneratorParamsException("roomNumbers "+temperatureGeneratorParameters.roomNumbers());
        if(temperatureGeneratorParameters.thermometerNumbers()<1) throw new BadTemperatureGeneratorParamsException("thermometerNumbers "+temperatureGeneratorParameters.thermometerNumbers());
        if(temperatureGeneratorParameters.dataSeries()<1) throw new BadTemperatureGeneratorParamsException("dataSeries "+temperatureGeneratorParameters.dataSeries());
        if(temperatureGeneratorParameters.meanValue()<20) throw new BadTemperatureGeneratorParamsException("meanValue "+temperatureGeneratorParameters.meanValue());
        if(temperatureGeneratorParameters.meanValue()<20) throw new BadTemperatureGeneratorParamsException("meanValue "+temperatureGeneratorParameters.meanValue());
        this.roomNumbers = temperatureGeneratorParameters.roomNumbers();
        this.thermometerNumbers = temperatureGeneratorParameters.thermometerNumbers();
        this.meanValue = temperatureGeneratorParameters.meanValue();
        this.dataSeries = temperatureGeneratorParameters.dataSeries();
        this.mode=temperatureGeneratorParameters.mode();
        this.anomalyOccurrences = temperatureGeneratorParameters.anomalyOccurrences();
        this.start = LocalDateTime.now();
        this.initRoomList();
    }
    private void initRoomList(){
        var roomIds = IntStream.rangeClosed(1, roomNumbers).boxed().toList();
        int fullRoomThermometerNumber = (this.thermometerNumbers / this.roomNumbers)+1;
        int lastRoomThermometerNumber = this.thermometerNumbers - fullRoomThermometerNumber*(this.roomNumbers-1);
        this.roomList  = roomIds.stream().map(id -> {
            int thermometerNumber = id.equals(roomIds.get(roomIds.size()-1)) ?lastRoomThermometerNumber:fullRoomThermometerNumber;
            double roomMeanValue = meanValue + (maxAllowDiff * (random.nextDouble() - 0.5));
            return new TemperatureGeneratorRoom(String.valueOf(id),thermometerNumber, roomMeanValue, this.maxAllowDiff / 2, this.readingTimeFrequency,start);
        }).toList();
    }
    @Override
    public List<TemperatureReading> generate() {
        List<TemperatureReading> generatedList = IntStream.range(0, dataSeries).mapToObj(i -> roomList.stream().map(TemperatureGeneratorRoom::generate)
                .flatMap(Collection::stream).toList()).flatMap(Collection::stream).toList();
        return generateAnomalies(generatedList);
    }
    private List<TemperatureReading> generateAnomalies(List<TemperatureReading> list){
        if(anomalyOccurrences>0){
            List<TemperatureReading> newList = new ArrayList<>(list);
            int step = newList.size()/anomalyOccurrences;
            if(step>0){
                for(int i=step/2;i<newList.size();i=i+step){
                    TemperatureReading temperatureReading = newList.get(i);
                    double newTemperature = meanValue + 3* maxAllowDiff;
                    TemperatureReading newTemperatureReading = new TemperatureReading(newTemperature,temperatureReading.roomId(),temperatureReading.thermometerId(),temperatureReading.timestamp());
                    newList.set(i,newTemperatureReading);
                }
                return newList;
            }
            else{
                return list;
            }

        }
        return list;
    }
}
