package io.kontak.apps.temperature.generator;

import io.kontak.apps.event.TemperatureReading;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

class TemperatureGeneratorThermometer {
    private String id;
    private String roomId;
    private double meanValue;
    private double maxAllowDiff;
    private int readingTimeFrequency;
    private LocalDateTime start;
    private final AtomicInteger counter = new AtomicInteger(0);
    private final Random random = new Random();

    public TemperatureGeneratorThermometer(String id,String roomId,LocalDateTime start, double meanValue,double maxAllowDiff,int readingTimeFrequency){
        this.id=id;
        this.meanValue=meanValue;
        this.maxAllowDiff=maxAllowDiff;
        this.readingTimeFrequency=readingTimeFrequency;
        this.roomId=roomId;
        this.start= start;
    }
    public TemperatureReading generateNext(){
        double temperature = meanValue + 0.2 * maxAllowDiff * (random.nextDouble()-0.5);
        return new TemperatureReading(temperature,roomId,id,start.plusSeconds((long) readingTimeFrequency *counter.getAndIncrement()).toInstant(ZoneOffset.UTC));
    }
}
