package io.kontak.apps.temperature.generator.dto;

public enum TemperatureGeneratorMode {
    TIME_SERIES,
    FRAME_SERIES
}
