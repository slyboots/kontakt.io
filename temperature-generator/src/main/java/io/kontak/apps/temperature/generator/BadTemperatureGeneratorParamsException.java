package io.kontak.apps.temperature.generator;

public class BadTemperatureGeneratorParamsException extends RuntimeException{
    public BadTemperatureGeneratorParamsException(String message){
        super("Incorrect params "+message);
    }
}
