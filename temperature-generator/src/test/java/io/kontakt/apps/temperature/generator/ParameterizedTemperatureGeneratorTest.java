package io.kontakt.apps.temperature.generator;

import io.kontak.apps.event.TemperatureReading;
import io.kontak.apps.temperature.generator.BadTemperatureGeneratorParamsException;
import io.kontak.apps.temperature.generator.ParameterizedTemperatureGenerator;
import io.kontak.apps.temperature.generator.TemperatureGenerator;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorMode;
import io.kontak.apps.temperature.generator.dto.TemperatureGeneratorParameters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import javax.management.BadAttributeValueExpException;

import static java.util.stream.Collectors.groupingBy;

public class ParameterizedTemperatureGeneratorTest {

    double meanValue = 20;
    int roomNumber = 3;
    int thermometerNumber = 14;
    int dataSeries = 4;
    int anomalyOccurrenceFrequency = 2;
    double maxAllowDiff = 5;
    int  readingTimeFrequency = 2;
    TemperatureGeneratorMode mode = TemperatureGeneratorMode.FRAME_SERIES;
    TemperatureGeneratorParameters parameters;
    @BeforeEach
    void setup(){
        this.parameters = new TemperatureGeneratorParameters(meanValue,roomNumber,thermometerNumber,dataSeries,
                anomalyOccurrenceFrequency,maxAllowDiff,readingTimeFrequency, mode);
    }

    @Test
    public void should_return_correct_reading_size_for_default_params(){
        //given
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then
        Assertions.assertNotNull(readings);
        Assertions.assertEquals(thermometerNumber*dataSeries,readings.size());
   }
    @Test
    public void should_return_reading_list_from_proper_thermometers(){
        //given
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then

        Assertions.assertEquals(thermometerNumber, readings.stream().map(TemperatureReading::thermometerId).distinct().toList().size());
    }
    @Test
    public void should_return_reading_list_from_proper_rooms(){
        //given
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then

        Assertions.assertEquals(roomNumber, readings.stream().map(TemperatureReading::roomId).distinct().toList().size());
    }
    @Test
    public void should_return_reading_list_with_correct_temperatures(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,roomNumber,thermometerNumber,dataSeries,
                0,maxAllowDiff,readingTimeFrequency, mode);
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then
        Assertions.assertEquals(thermometerNumber*dataSeries, readings.stream().map(TemperatureReading::temperature)
                .filter(temperature->temperature>meanValue-maxAllowDiff).filter(temperature->temperature<meanValue+maxAllowDiff).toList().size());
    }
    @Test
    public void should_return_reading_list_with_correct_amount_anomalies(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,roomNumber,thermometerNumber,dataSeries,
                2,maxAllowDiff,readingTimeFrequency, mode);
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then
        Assertions.assertEquals(2, readings.stream().map(TemperatureReading::temperature)
                .filter(temperature->temperature>meanValue+maxAllowDiff|| temperature<meanValue-maxAllowDiff).toList().size());
    }
    @Test
    public void should_return_correct_reading_size_for_edge_params(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,1,1,1,
                anomalyOccurrenceFrequency,maxAllowDiff,readingTimeFrequency, mode);
        TemperatureGenerator temperatureGenerator = new ParameterizedTemperatureGenerator(parameters);
        //when
        var readings =  temperatureGenerator.generate();
        //then
        Assertions.assertNotNull(readings);
        Assertions.assertEquals(1,readings.size());
    }
    @Test
    public void should_throw_exception_for_incorrect_room_numbers(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,-1,1,1,
                anomalyOccurrenceFrequency,maxAllowDiff,readingTimeFrequency, mode);
        //when
        BadTemperatureGeneratorParamsException thrown = Assertions.assertThrows(BadTemperatureGeneratorParamsException.class, () -> {
            new ParameterizedTemperatureGenerator(parameters);
        });
        //then
        Assertions.assertEquals("Incorrect params roomNumbers -1", thrown.getMessage());
    }
    @Test
    public void should_throw_exception_for_incorrect_thermometer_numbers(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,1,-1,1,
                anomalyOccurrenceFrequency,maxAllowDiff,readingTimeFrequency, mode);
        //when
        BadTemperatureGeneratorParamsException thrown = Assertions.assertThrows(BadTemperatureGeneratorParamsException.class, () -> {
            new ParameterizedTemperatureGenerator(parameters);
        });
        //then
        Assertions.assertEquals("Incorrect params thermometerNumbers -1", thrown.getMessage());
    }
    @Test
    public void should_throw_exception_for_incorrect_data_series(){
        //given
        this.parameters = new TemperatureGeneratorParameters(meanValue,1,1,-1,
                anomalyOccurrenceFrequency,maxAllowDiff,readingTimeFrequency, mode);
        //when
        BadTemperatureGeneratorParamsException thrown = Assertions.assertThrows(BadTemperatureGeneratorParamsException.class, () -> {
            new ParameterizedTemperatureGenerator(parameters);
        });
        //then
        Assertions.assertEquals("Incorrect params dataSeries -1", thrown.getMessage());
    }
}
